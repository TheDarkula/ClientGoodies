#[allow(unused_imports)]
use determineDistance;

#[test]
fn determineDistanceTest() {
    assert_eq!(determineDistance(52.986375, -6.043701), 41.76872550078046);
}
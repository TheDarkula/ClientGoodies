#![allow(non_snake_case, non_upper_case_globals, non_camel_case_types)]

extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

use std::io;
use std::io::prelude::*;
use std::error::Error;
use std::io::Stdin;

mod tests;

pub const intercomLatitude: f64 = 53.339428;
pub const intercomLongitude: f64 = -6.257664;
pub const earthRadius: f64 = 6371.0;

#[derive(Serialize, Deserialize, Debug)]
struct fullInfo {
    name: String,
    user_id: i32,
    latitude: f64,
    longitude: f64,
}

fn parseInfo(inputString: &str) -> Result<fullInfo, Box<Error>> {
    let singleLine = serde_json::from_str(&inputString)?;
    Ok(singleLine)
}

fn createClientList(input: Stdin) -> Vec<fullInfo> {
    let mut indexToUnwrap;
    let mut indexToPush;

    let mut clientList = Vec::new();

    for line in input.lock().lines() {
        indexToUnwrap = parseInfo(&line.expect("Didn't receive valid input"));
        indexToPush = indexToUnwrap.unwrap();
        clientList.push(indexToPush);
    }
    clientList.sort_by_key(|instance| instance.user_id);
    clientList
}

fn determineDistance(clientLatitude: f64, clientLongitude: f64) -> f64 {
    let clientLatitudeRadians = clientLatitude.to_radians();
    let clientLongitudeRadians = clientLongitude.to_radians();

    let intercomLatitudeRadians = intercomLatitude.to_radians();
    let intercomLongitudeRadians = intercomLongitude.to_radians();

    let absoluteLongitudeDifference = (clientLongitudeRadians - intercomLongitudeRadians).abs();
    let centralAngle = (clientLatitudeRadians.sin() * intercomLatitudeRadians.sin() + clientLatitudeRadians.cos() * intercomLatitudeRadians.cos() * absoluteLongitudeDifference.cos()).acos();
    let finalDistance = earthRadius * centralAngle;

    finalDistance
}

fn finalResult(clientList: Vec<fullInfo>) {
    for index in clientList.iter() {
        if determineDistance(index.latitude, index.longitude) <= 100.0 {
            println!("Name: {}, User ID: {}", index.name, index.user_id);
        }
    }
}

fn main() {
    let stdin = io::stdin();

    let clientList = createClientList(stdin);

    finalResult(clientList);
}
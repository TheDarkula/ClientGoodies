# Client Goodies
Client Goodies is a program that takes a single-line delitimed json input from STDIN. It sorts that file into a vector, arranged ascendingly, by user_id. It then outputs all clients' names and user_id's that are within 100 kilometres of the Intercom office in Dublin.

Written in Rust


## Building:
  You will need cargo.

  To run:

    cat src/clients.json | sed -e 's/"\([0-9]\+\.[0-9]\+\)"/\1/g' -e 's/"\(-[0-9]\+\.[0-9]\+\)"/\1/g' -e 's/"\([0-9]\+\)"/\1/g' -e 's/"\(-[0-9]\+\)"/\1/g' | cargo run release


## Tests:
  Just run:

    cargo test


## Sources:
  * ["Great-circle distance"](https://en.wikipedia.org/wiki/Great-circle_distance) - Wikipedia.org, Modified: January 2018